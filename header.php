<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package hondabacninh
 * @subpackage hondabacninh
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php if ( pd_option( 'responsive', true, false ) ) : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php endif; ?>
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" <?php pd_layout_classes( 'site' ); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html__( 'Skip to content', 'hondabacninh' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<div class="site-topbar">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-sm-5 col-xs-8 topbar-left">
						<div class="topbar_mail">
							<a href="mailto:maycuanhom.bp@gmail.com"><i class="fa fa-envelope"></i> maycuanhom.bp@gmail.com</a>
						</div>
					</div>
					<div class="col-md-7 col-sm-7 col-xs-3 topbar-right">
						<div class="row">
							<div class=" col-xs-12 topbar_social  no-padding text-right">
								<a class="facebook" href="https://www.facebook.com/" rel="external nofollow" target="_blank"><i class="fa fa-facebook"></i></a>
								<a class="share-google ext-link" href="http://google.com" rel="external nofollow" target="_blank"><i class="fa fa-google"></i></a>
								<a class="share-twitter ext-link" href="http://twitter.com" rel="external nofollow" target="_blank"><i class="fa fa-twitter"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- .site-topbar -->
		<div class="site-branding"<?php echo (get_header_image() && has_custom_logo()) ? ' style="background: url(' . get_header_image() . ') center center no-repeat;"' : ''; ?>>
			<?php if (get_header_image() && !has_custom_logo() ) : ?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo get_header_image(); ?>" alt="<?php echo get_bloginfo( 'description', 'display' ) ?>">
				</a>
			<?php else : ?>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php if ( has_custom_logo() && get_header_image() ) : ?>
								<?php the_custom_logo(); ?>
							<?php else : ?>
								<?php
								if ( ! is_singular() || is_front_page() ) : ?>
									<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
								<?php else : ?>
									<div class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></div>
								<?php
								endif;

								$description = get_bloginfo( 'description', 'display' );
								if ( $description || is_customize_preview() ) : ?>
									<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
								<?php
								endif; ?>
							<?php endif; ?>
						</div><!-- .col-xs-4 -->

						<?php if ( is_active_sidebar( 'header-right' ) ) : ?>
						<div class="col-md-8 col-sm-12 col-xs-12">
							<?php dynamic_sidebar( 'header-right' ); ?>
						</div>
						<?php endif; ?>

					</div><!-- .row -->
				</div><!-- .container -->
			<?php endif; ?>
		</div><!-- .site-branding -->

		<?php get_template_part( 'template-parts/navigation' ); ?>

	</header><!-- #masthead -->

	<div id="content" class="site-content">

		<?php do_action( 'above_content_before' ); ?>

		<?php if ( is_active_sidebar( 'above-content' ) && is_front_page() ) : ?>
		<div class="above-content-section">
			<?php if ( ! pd_option( 'above_content_full_width', null, false ) ) : ?>
			<div class="container">
				<div class="row">
			<?php endif; ?>

					<?php dynamic_sidebar( 'above-content' ); ?>

			<?php if ( ! pd_option( 'above_content_full_width', null, false ) ) : ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<?php do_action( 'above_content_after' ); ?>

		<div class="container">
			<div class="row">
				<div id="layout" <?php pd_layout_class( 'clearfix' ); ?>>

					<?php do_action( 'before_layout' );
