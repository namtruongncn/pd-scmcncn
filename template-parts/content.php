<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package hondabacninh
 * @subpackage hondabacninh
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail col-xs-5">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'full' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>
	<header class="entry-header">

		<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>

		<div class="entry-meta">
			<?php pd_posted_on(); ?>
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->


	<div class="entry-content">
		<?php
			the_excerpt();

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'rt' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
