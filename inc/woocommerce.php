<?php
/**
 * Woocommerce functions.
 *
 * @package hondabacninh.
 */

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'pd_woocommerce_template_loop_product_thumbnail', 10 );

//add_action( 'woocommerce_after_shop_loop_item_title', 'pd_woocommerce_product_excerpt', 20 );

remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'pd_woocommerce_template_loop_product_title', 10 );

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
//add_action( 'woocommerce_after_shop_loop_item_title', 'pd_woocommerce_template_loop_order', 15 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 6 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

add_action( 'woocommerce_single_product_summary', 'pd_qv_woocommerce_template_single_add_to_cart', 30 );

remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

// Breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
//add_action( 'woocommerce_before_single_product', 'woocommerce_breadcrumb', 5 );

add_action( 'tooltip_woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'tooltip_woocommerce_before_shop_loop_item_title', 'pd_tooltip_woocommerce_template_loop_product_thumbnail', 15 );

if ( ! function_exists( 'pd_woocommerce_get_product_thumbnail' ) ) {

	/**
	 * Get the product thumbnail, or the placeholder if not set.
	 *
	 * @subpackage	Loop
	 * @param string $size (default: 'shop_catalog')
	 * @param int $deprecated1 Deprecated since WooCommerce 2.0 (default: 0)
	 * @param int $deprecated2 Deprecated since WooCommerce 2.0 (default: 0)
	 * @return string
	 */
	function pd_woocommerce_get_product_thumbnail( $size = 'full', $deprecated1 = 0, $deprecated2 = 0 ) {
		global $post;
		$image_size = apply_filters( 'pd_single_product_archive_thumbnail_size', $size );

		if ( has_post_thumbnail() ) {
			$props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			return get_the_post_thumbnail( $post->ID, $image_size, array(
				'alt'    => $props['alt'],
			) );
		} elseif ( wc_placeholder_img_src() ) {
			return wc_placeholder_img( $image_size );
		}
	}
}

/**
 * pd_woocommerce_template_loop_product_thumbnail
 *
 * @return [type] [description]
 */
function pd_woocommerce_template_loop_product_thumbnail() {
	echo '<a href="' . get_the_permalink() . '">' . pd_woocommerce_get_product_thumbnail() . '</a>';
}

/**
 * pd_tooltip_woocommerce_template_loop_product_thumbnail
 *
 * @return [type] [description]
 */
function pd_tooltip_woocommerce_template_loop_product_thumbnail() {
	echo "<a class='pd-tooltip' data-tooltip='{\"image\": \"" . esc_attr( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) . "\"}' href='" . get_the_permalink() . "'>" . pd_woocommerce_get_product_thumbnail() . "</a>";
}

/**
 * [pd_add_to_cart_text description]
 * @return [type] [description]
 */
function pd_add_to_cart_text() {
	return apply_filters( 'pd_woocommerce_product_add_to_cart_text', esc_html__( 'Đặt mua', 'hondabacninh' ) );
}
add_filter( 'woocommerce_product_add_to_cart_text', 'pd_add_to_cart_text' );

/**
 * [pd_woocommerce_product_single_add_to_cart_text description]
 * @return [type] [description]
 */
function pd_woocommerce_product_single_add_to_cart_text() {
	return apply_filters( 'pd_woocommerce_product_single_add_to_cart_text', esc_html__( 'Mua ngay', 'hondabacninh' ) );
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'pd_woocommerce_product_single_add_to_cart_text' );

/**
 * [pd_woocommerce_product_excerpt description]
 * @return [type] [description]
 */
function pd_woocommerce_product_excerpt() {
	echo '<div class="pd_product_excerpt">';
	echo wp_trim_words( get_the_content(), 13, ' ...' );
	echo '</div>';
}

if ( ! function_exists( 'pd_woocommerce_template_loop_product_title' ) ) {

	/**
	 * Show the product title in the product loop. By default this is an H3.
	 */
	function pd_woocommerce_template_loop_product_title() {
		echo '<h2 class="pd_woocommerce-loop-product__title"><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h2>';
	}
}

/**
 * [pd_woocommerce_template_loop_order description]
 * @return [type] [description]
 */
function pd_woocommerce_template_loop_order() {
	echo '<div class="pd_rating"></div>';
}

/**
 * [pd_qv_woocommerce_template_single_title description]
 * @return [type] [description]
 */
function pd_qv_woocommerce_template_single_title() {
	the_title( '<h3 class="product_title entry-title"><a href="' . get_the_permalink() . '">', '</a></h3>' );
}

/**
 * [pd_qv_woocommerce_template_single_price description]
 * @param  [type] $product [description]
 * @return [type]          [description]
 */
function pd_qv_woocommerce_template_single_price() {
	global $product;

	$regular_price = $product->get_regular_price();
	$sale_price = $sale_price = $product->get_sale_price();

	if ( ! empty( $regular_price ) ) : ?>
	<span class="price<?php echo $sale_price ? '' : ' no-sale-price' ?>">
		<?php if ( ! empty( $sale_price ) ) : ?>
		<span class="sale-price">
			<?php printf( '%s₫', number_format_i18n( $sale_price ) ); ?>
		</span>

		<?php endif; ?>
		<span class="regular-price">
			<?php printf( '%s₫', number_format_i18n( $regular_price ) ); ?>
		</span>
	</span>
	<?php
	endif;
}

/**
 * [pd_qv_woocommerce_template_single_excerpt description]
 * @return [type] [description]
 */
function pd_qv_woocommerce_template_single_excerpt() {
	global $post;

	if ( ! $post->post_excerpt ) {
		return;
	}
	printf( '<div class="woocommerce-product-details__short-description"><strong>%1$s</strong>%2$s</div>', esc_html__( 'Mô tả:', 'hondabacninh' ), wp_trim_words( get_the_excerpt(), 40, '<a href="' . get_the_permalink() . '" title="' . esc_html__( 'Xem chi tiết sản phẩm này', 'hondabacninh' ) . '">' . esc_html__( '...Xem chi tiết', 'hondabacninh' ) . '</a>' ) );
}

/**
 * Single add to cart template.
 *
 * @return string single add to cart template.
 */
function pd_qv_woocommerce_template_single_add_to_cart() { ?>
	<form class="cart" method="post" enctype="multipart/form-data">
		<div class="quantity_wanted_p">
			<div class="quantity">
				<label for="quantity-detail" class="quantity-selector slg_g">Số lượng</label>
				<a class="btn_num button_qty" onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty > 1 ) result.value--;return false;" type="button">-</a>
				<input id="qty" type="text" class="input-text qty text" name="quantity" value="1" title="SL">
				<a class="btn_num button_qty" onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" type="button">+</a>
			</div>
		</div>

		<button type="submit" name="add-to-cart" value="<?php the_ID(); ?>" class="pd_qv_btn"><?php esc_html_e( 'Thêm vào giỏ', 'hondabacninh' ); ?></button>

	</form>
<?php
}

/**
 * Product product images teamplate.
 *
 * @return string Product Images template.
 */
function pd_qv_woocommerce_show_product_images() {
	global $post, $product;

	$attachment_ids = $product->get_gallery_image_ids();

	if ( $attachment_ids && has_post_thumbnail() ) { ?>
		<div class="pd_galleries_products images">
			<div class="pd_product_thumbnails">
				<?php
				foreach ( $attachment_ids as $attachment_id ) {
					$full_size_image  = wp_get_attachment_image_src( $attachment_id, 'full' );
					$thumbnail        = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
					$thumbnail_post   = get_post( $attachment_id );
					$image_title      = $thumbnail_post->post_content;

					$attributes = array(
						'title'                   => $image_title,
						'data-src'                => $full_size_image[0],
						'data-large_image'        => $full_size_image[0],
						'data-large_image_width'  => $full_size_image[1],
						'data-large_image_height' => $full_size_image[2],
					);

					$html  = '<div class="pd-product-gallery__image">';
					$html .= wp_get_attachment_image( $attachment_id, 'shop_single', false, $attributes );
					$html .= '</div>';

					echo $html;
				} ?>
			</div>
			<div class="pd_product_thumbnails_gallery">
				<?php
				foreach ( $attachment_ids as $attachment_id ) {
					$full_size_image  = wp_get_attachment_image_src( $attachment_id, 'full' );
					$thumbnail        = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
					$thumbnail_post   = get_post( $attachment_id );
					$image_title      = $thumbnail_post->post_content;

					$attributes = array(
						'title'                   => $image_title,
						'data-src'                => $full_size_image[0],
						'data-large_image'        => $full_size_image[0],
						'data-large_image_width'  => $full_size_image[1],
						'data-large_image_height' => $full_size_image[2],
					);

					$html  = '<div class="pd-product-gallery__image">';
					$html .= wp_get_attachment_image( $attachment_id, 'shop_single', false, $attributes );
					$html .= '</div>';

					echo $html;
				} ?>
			</div>
		</div>
	<?php
	}
}

/**
 * Single Product summary template.
 *
 * @return string Woocommerce single product summary html template.
 */
function pd_woocommerce_single_product_summary() { 
	?>

	<div class="pd_woocommerce_single_product_summary clearfix">
		<div class="pd_woocommerce_single_product_summary-left">
			<?php woocommerce_template_single_excerpt(); ?>
			<?php woocommerce_template_single_price(); ?>
			<div class="pd_box_callback">
				<form id="pd_box_callback" class="clearfix" method="post" action="<?php the_permalink(); ?>">
					<label for="pd_callback_phone"><?php esc_html_e( 'Chúng tôi sẽ gọi lại cho bạn:', 'hondabacninh' ); ?></label>
					<input id="pd_callback_phone" type="text" name="phone" placeholder="<?php esc_html_e( 'Nhập số điện thoại', 'hondabacninh' ); ?>">
					<input type="submit" value="<?php esc_html_e( 'Gửi', 'hondabacninh' ); ?>">
				</form>

				<?php if ( ! empty( $_POST['phone'] ) ) {
					$to = pd_option( 'pd_callback_email', null, false );
					$subject = pd_option( 'pd_callback_subject', null, false );
					$message = pd_option( 'pd_callback_message', null, false ) . ': ' . $_POST['phone'];
					$headers = array('Content-Type: text/html; charset=UTF-8');

					wp_mail( $to, $subject, $message, $headers );
				} ?>
			</div>
		</div>
	</div>
	<div class="psupport"><?php pd_option( 'pd_product_hotline' ); ?></div>
<?php
}
//add_action( 'woocommerce_single_product_summary', 'pd_woocommerce_single_product_summary', 20 );

/**
 * Tabs of content product.
 *
 * @param  array  $tabs Tabs of content.
 * @return array       //
 */
function pd_woocommerce_product_tabs( $tabs = array() ) {
	global $product, $post;

	// Description tab - shows product content
	if ( $post->post_content ) {
		$tabs['description'] = array(
			'title'    => esc_html__( 'Mô tả sản phẩm', 'hondabacninh' ),
			'priority' => 10,
			'callback' => 'woocommerce_product_description_tab',
		);
	}

	unset($tabs['reviews']);

	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'pd_woocommerce_product_tabs' );

/**
 * Under Singular Sidebar.
 */
function pd_under_singular_sidebar() {
	if ( is_active_sidebar( 'under-singular' ) ) {
		dynamic_sidebar( 'under-singular' );
	}
}
add_action( 'woocommerce_after_single_product_summary', 'pd_under_singular_sidebar', 16 );

if ( pd_option( 'buy_now_btn', null, false ) ) {
	/**
	 * [pd_enable_disable_buynow_btn description]
	 * @param  array  $args [description]
	 * @return [type]       [description]
	 */
	function pd_enable_disable_buynow_btn( $args = array() ) {
		global $product;

		if ( $product ) {
			$defaults = array(
				'quantity' => 1,
				'class'    => implode( ' ', array_filter( array(
						'button',
						'product_type_' . $product->get_type(),
						$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
						$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
				) ) ),
			);

			$args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

			echo apply_filters( 'woocommerce_loop_add_to_cart_link',
				sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
					esc_url( $product->add_to_cart_url() ),
					esc_attr( isset( $quantity ) ? $quantity : 1 ),
					esc_attr( $product->get_id() ),
					esc_attr( $product->get_sku() ),
					esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
					esc_html( $product->add_to_cart_text() )
				),
			$product );
		}
	}
	add_action( 'pd_add_to_cart', 'pd_enable_disable_buynow_btn', 10 );
}

function pd_woocommerce_output_related_products_args( $args ) {
	$args = array(
		'posts_per_page' => pd_option( 'related_product_items', null, false ),
		'columns'        => 4,
		'orderby'        => 'rand',
	);

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'pd_woocommerce_output_related_products_args' );

add_filter( 'woocommerce_product_tabs', 'pd_edit_product_tabs', 98 );

function pd_edit_product_tabs( $tabs ) {

    unset( $tabs['reviews'] );      // Remove the reviews tab
                                    // 
    $tabs['comment']['title'] = __( 'Bình luận', 'phoenixdigi' );
	$tabs['comment']['priority']= 60;
	$tabs['comment']['callback']='content_tab_comment';
    return $tabs;

}
function content_tab_comment()
{ ?>
	<div class="fb-comments" data-href="<?php echo get_the_permalink(); ?>" data-numposts="5"></div>
<?php }