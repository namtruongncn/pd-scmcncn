<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * @see https://github.com/TGMPA/TGM-Plugin-Activation/blob/develop/example.php
 *
 * @package RTCORRE
 */

/**
 * Register the required plugins for this theme.
 */
function hondabacninh_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 */
	$plugins = array(
		array(
			'name'     => esc_html__( 'PhoenixDigi Core', 'hondabacninh' ),
			'slug'     => 'pd-scmcncn-plugin',
			'source'   => get_stylesheet_directory() . '/plugins/pd-plugin.zip',
			'required' => true,
		),
		array(
			'name'     => esc_html__( 'Meta Slider', 'hondabacninh' ),
			'slug'     => 'ml-slider',
			'required' => true,
		),
		array(
			'name'     => esc_html__( 'WooCommerce', 'hondabacninh' ),
			'slug'     => 'woocommerce',
			'required' => true,
		),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 */
	$config = array(
		'id'           => 'hondabacninh',
		'is_automatic' => true,
		'strings'      => array( 'nag_type' => 'notice-warning' ),
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'hondabacninh_register_required_plugins' );
