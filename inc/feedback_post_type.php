<?php 
// Register Custom Post Type
function feed_back_post_type() {

	$labels = array(
		'name'                  => _x( 'Ý kiến khách hàng', 'Ý kiến khách hàng', 'phoenixdigi' ),
		'singular_name'         => _x( 'Ý kiến khách hàng', 'Ý kiến khách hàng', 'phoenixdigi' ),
		'menu_name'             => __( 'Ý kiến khách hàng', 'phoenixdigi' ),
		'name_admin_bar'        => __( 'Ý kiến khách hàng', 'phoenixdigi' ),
	);
	$args = array(
		'label'                 => __( 'Ý kiến khách hàng', 'phoenixdigi' ),
		'description'           => __( 'Hiển thị thông tin phản hồi từ khách hàng', 'phoenixdigi' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'feed_cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'feedback', $args );

}
add_action( 'init', 'feed_back_post_type', 0 );